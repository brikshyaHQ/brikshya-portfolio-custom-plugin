<?php

/**
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              brikshya.com
 * @since             1.0.0
 * @package           Brikshya_Portfolio_Cpt
 *
 * @wordpress-plugin
 * Plugin Name:       Brikshya Portfolio CPT
 * Plugin URI:        brikshya.com/plugins/brikshya-portfolio-cpt
 * Description:       This plugin is exclusively for brikshya portfolio theme. This plugin generates require custom post types and their relative taxonomy.
 * Version:           1.0.0
 * Author:            Brikshya
 * Author URI:        brikshya.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       brikshya-portfolio-cpt
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if (!defined('WPINC')) {
    die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define('BRIKSHYA_PORTFOLIO_CPT_VERSION', '1.0.0');

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-brikshya-portfolio-cpt-activator.php
 */
function activate_brikshya_portfolio_cpt()
{
    require_once plugin_dir_path(__FILE__) . 'includes/class-brikshya-portfolio-cpt-activator.php';
    Brikshya_Portfolio_Cpt_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-brikshya-portfolio-cpt-deactivator.php
 */
function deactivate_brikshya_portfolio_cpt()
{
    require_once plugin_dir_path(__FILE__) . 'includes/class-brikshya-portfolio-cpt-deactivator.php';
    Brikshya_Portfolio_Cpt_Deactivator::deactivate();
}

register_activation_hook(__FILE__, 'activate_brikshya_portfolio_cpt');
register_deactivation_hook(__FILE__, 'deactivate_brikshya_portfolio_cpt');

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path(__FILE__) . 'includes/class-brikshya-portfolio-cpt.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_brikshya_portfolio_cpt()
{

    $plugin = new Brikshya_Portfolio_Cpt();
    $plugin->run();

}

run_brikshya_portfolio_cpt();

/*-----------------------------------------------------------------------------------*/
/* Custom Post type blog
/*-----------------------------------------------------------------------------------*/
if (!function_exists('brikshya_portfolio_custom_post_type_blog')):
    function brikshya_portfolio_custom_post_type_blog()
    {
        $labels = array(
            'name' => _x('Blogs', 'Post Type General Name', 'brikshya-portfolio'),
            'singular_name' => _x('Blog', 'Post Type General Name', 'brikshya-portfolio'),
            'add_new' => __('Add Item', 'brikshya-portfolio'),
            'all_items' => __('All Items', 'brikshya-portfolio'),
            'add_new_item' => __('Add Item', 'brikshya-portfolio'),
            'edit_item' => __('Edit Item', 'brikshya-portfolio'),
            'new_item' => __('New Item', 'brikshya-portfolio'),
            'view_item' => __('View Item', 'brikshya-portfolio'),
            'search_item' => __('Search', 'brikshya-portfolio'),
            'not_found' => __('No Item Found', 'brikshya-portfolio'),
            'not_found_in_trash' => __('No Item Found In Trash', 'brikshya-portfolio'),
            'parent_item_colon' => __('Parent Item', 'brikshya-portfolio'),
        );
        $args = array(
            'labels' => $labels,
            'public' => true,
            'has_archive' => true,
            'publicly_queryable' => true,
            'query_var' => true,
            'rewrite' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'supports' => array(
                'title',
                'editor',
                'thumbnail',
                'revisions',
                'comments'
            ),
            'show_in_rest' => true,
            'menu_position' => 5,
            'menu_icon' => 'dashicons-welcome-write-blog',
            'exclude_from_search' => false
        );
        register_post_type('blogs', $args);
    }
endif;
add_action('init', 'brikshya_portfolio_custom_post_type_blog');


/*-----------------------------------------------------------------------------------*/
/* Taxonomy for Custom Post type blog
/*-----------------------------------------------------------------------------------*/
if (!function_exists('brikshya_portfolio_custom_taxonomies_blog')):
    function brikshya_portfolio_custom_taxonomies_blog()
    {
        //add new taxonomy hierarchical
        $labels = array(
            'name' => _x('Blog Categories', 'Taxonomy Type General Name', 'brikshya-portfolio'),
            'singular_name' => _x('Blog Category', 'Taxonomy Type General Name', 'brikshya-portfolio'),
            'search_items' => __('Search Blog Categories', 'brikshya-portfolio'),
            'all_items' => __('All Blog Categories', 'brikshya-portfolio'),
            'parent_item' => __('Parent Blog Categories', 'brikshya-portfolio'),
            'parent_item_colon' => __('Parent Blog Category', 'brikshya-portfolio'),
            'edit_item' => __('Edit Blog Categories', 'brikshya-portfolio'),
            'update_item' => __('Update Blog Categories', 'brikshya-portfolio'),
            'add_new_item' => __('Add New Blog Categories', 'brikshya-portfolio'),
            'new_item_name' => __('New Blog Categories', 'brikshya-portfolio'),
            'menu_name' => __('Blog Categories', 'brikshya-portfolio'),
        );

        $args = array(
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'show_in_rest' => true, // Needed for tax to appear in Gutenberg editor
            'query_var' => true,
            'rewrite' => array('slug' => 'blog-category')
        );
        register_taxonomy('blog-category', array('blogs'), $args);


        //add new taxonomy NOT hierarchical

        register_taxonomy('blog-tags', 'blogs', array(
            'label' => 'Blog tags',
            'show_in_rest' => true, // Needed for tax to appear in Gutenberg editor
            'rewrite' => array('slug' => 'blog-tag'),
            'hierarchical' => false
        ));
    }
endif;
add_action('init', 'brikshya_portfolio_custom_taxonomies_blog');

/*-----------------------------------------------------------------------------------*/
/* Custom Post type Experience
/*-----------------------------------------------------------------------------------*/
if (!function_exists('brikshya_portfolio_custom_post_type_experience')):
    function brikshya_portfolio_custom_post_type_experience()
    {
        $labels = array(
            'name' => _x('Eperiences', 'Post Type General Name', 'brikshya-portfolio'),
            'singular_name' => _x('Eperience', 'Post Type General Name', 'brikshya-portfolio'),
            'add_new' => __('Add Item', 'brikshya-portfolio'),
            'all_items' => __('All Items', 'brikshya-portfolio'),
            'add_new_item' => __('Add Item', 'brikshya-portfolio'),
            'edit_item' => __('Edit Item', 'brikshya-portfolio'),
            'new_item' => __('New Item', 'brikshya-portfolio'),
            'view_item' => __('View Item', 'brikshya-portfolio'),
            'search_item' => __('Search', 'brikshya-portfolio'),
            'not_found' => __('No Item Found', 'brikshya-portfolio'),
            'not_found_in_trash' => __('No Item Found In Trash', 'brikshya-portfolio'),
            'parent_item_colon' => __('Parent Item', 'brikshya-portfolio'),
        );
        $args = array(
            'labels' => $labels,
            'public' => true,
            'has_archive' => true,
            'publicly_queryable' => true,
            'query_var' => true,
            'rewrite' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'supports' => array(
                'title',
                'editor',
                'thumbnail',
                'revisions',
            ),
            'show_in_rest' => true,
            'menu_position' => 5,
            'menu_icon' => 'dashicons-text-page',
            'exclude_from_search' => false
        );
        register_post_type('experiences', $args);
    }
endif;
add_action('init', 'brikshya_portfolio_custom_post_type_experience');


/*-----------------------------------------------------------------------------------*/
/* Custom Post type services
/*-----------------------------------------------------------------------------------*/

if (!function_exists('brikshya_portfolio_custom_post_type_services')):
    function brikshya_portfolio_custom_post_type_services()
    {
        $labels = array(
            'name' => _x('Services', 'Post Type General Name', 'brikshya-portfolio'),
            'singular_name' => _x('Service', 'Post Type General Name', 'brikshya-portfolio'),
            'add_new' => __('Add Item', 'brikshya-portfolio'),
            'all_items' => __('All Items', 'brikshya-portfolio'),
            'add_new_item' => __('Add Item', 'brikshya-portfolio'),
            'edit_item' => __('Edit Item', 'brikshya-portfolio'),
            'new_item' => __('New Item', 'brikshya-portfolio'),
            'view_item' => __('View Item', 'brikshya-portfolio'),
            'search_item' => __('Search', 'brikshya-portfolio'),
            'not_found' => __('No Item Found', 'brikshya-portfolio'),
            'not_found_in_trash' => __('No Item Found In Trash', 'brikshya-portfolio'),
            'parent_item_colon' => __('Parent Item', 'brikshya-portfolio'),
        );
        $args = array(
            'labels' => $labels,
            'public' => true,
            'has_archive' => true,
            'publicly_queryable' => true,
            'query_var' => true,
            'rewrite' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'supports' => array(
                'title',
                'editor',
                'thumbnail',
                'revisions',
            ),
            'show_in_rest' => true,
            'menu_position' => 5,
            'menu_icon' => 'dashicons-list-view',
            'exclude_from_search' => false
        );
        register_post_type('services', $args);
    }
endif;
add_action('init', 'brikshya_portfolio_custom_post_type_services');


/*-----------------------------------------------------------------------------------*/
/* Custom Post type portfolio
/*-----------------------------------------------------------------------------------*/

if (!function_exists('brikshya_portfolio_custom_post_type_portfolio')):
    function brikshya_portfolio_custom_post_type_portfolio()
    {
        $labels = array(
            'name' => _x('Portfolio', 'Post Type General Name', 'brikshya-portfolio'),
            'singular_name' => _x('Portfolio', 'Post Type General Name', 'brikshya-portfolio'),
            'add_new' => __('Add Item', 'brikshya-portfolio'),
            'all_items' => __('All Items', 'brikshya-portfolio'),
            'add_new_item' => __('Add Item', 'brikshya-portfolio'),
            'edit_item' => __('Edit Item', 'brikshya-portfolio'),
            'new_item' => __('New Item', 'brikshya-portfolio'),
            'view_item' => __('View Item', 'brikshya-portfolio'),
            'search_item' => __('Search', 'brikshya-portfolio'),
            'not_found' => __('No Item Found', 'brikshya-portfolio'),
            'not_found_in_trash' => __('No Item Found In Trash', 'brikshya-portfolio'),
            'parent_item_colon' => __('Parent Item', 'brikshya-portfolio'),
        );
        $args = array(
            'labels' => $labels,
            'public' => true,
            'has_archive' => true,
            'publicly_queryable' => true,
            'query_var' => true,
            'rewrite' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'supports' => array(
                'title',
                'editor',
                'thumbnail',
                'revisions',
            ),
            'show_in_rest' => true,
            'menu_position' => 5,
            'menu_icon' => 'dashicons-portfolio',
            'exclude_from_search' => false
        );
        register_post_type('portfolio', $args);
    }
endif;
add_action('init', 'brikshya_portfolio_custom_post_type_portfolio');


/*-----------------------------------------------------------------------------------*/
/* Taxonomy for Custom Post type portfolio
/*-----------------------------------------------------------------------------------*/
if (!function_exists('brikshya_portfolio_custom_taxonomies_portfolio')):
    function brikshya_portfolio_custom_taxonomies_portfolio()
    {
        //add new taxonomy hierarchical
        $labels = array(
            'name' => _x('Portfolio Categories', 'Taxonomy Type General Name', 'brikshya-portfolio'),
            'singular_name' => _x('Portfolio Category', 'Taxonomy Type General Name', 'brikshya-portfolio'),
            'search_items' => __('Search Portfolio Categories', 'brikshya-portfolio'),
            'all_items' => __('All Portfolio Categories', 'brikshya-portfolio'),
            'parent_item' => __('Parent Portfolio Categories', 'brikshya-portfolio'),
            'parent_item_colon' => __('Parent Portfolio Category', 'brikshya-portfolio'),
            'edit_item' => __('Edit Portfolio Categories', 'brikshya-portfolio'),
            'update_item' => __('Update Portfolio Categories', 'brikshya-portfolio'),
            'add_new_item' => __('Add New Portfolio Categories', 'brikshya-portfolio'),
            'new_item_name' => __('New Portfolio Categories', 'brikshya-portfolio'),
            'menu_name' => __('Portfolio Categories', 'brikshya-portfolio'),
        );

        $args = array(
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'show_in_rest' => true, // Needed for tax to appear in Gutenberg editor
            'query_var' => true,
            'rewrite' => array('slug' => 'portfolio-category')
        );
        register_taxonomy('portfolio-category', array('portfolio'), $args);


        //add new taxonomy NOT hierarchical

        register_taxonomy('portfolio-tags', 'portfolio', array(
            'label' => 'Portfolio tags',
            'show_in_rest' => true, // Needed for tax to appear in Gutenberg editor
            'rewrite' => array('slug' => 'portfolio-tag'),
            'hierarchical' => false
        ));
    }
endif;
add_action('init', 'brikshya_portfolio_custom_taxonomies_portfolio');


if(!class_exists('Brikshya_Portolio_blog_posts')) {
    class Brikshya_Portolio_blog_posts extends WP_Widget
    {

        public function __construct()
        {
            $widget_options = array(
                'classname' => 'brikshya-portfolio-widget',
                'description' => 'Custom Brikshya Portfolio Widget',
            );
            parent::__construct('brikshya_blog_post', 'Brikshya Blog Post', $widget_options);
        }

        // back-end Display of widget
        public function form($instance)
        {
            if (isset($instance['title'])) {
                $title = $instance['title'];
            } else {
                $title = __('Resent Posts', 'brikshya-portfolio');
            }
// Widget admin form
            ?>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php _e('Title:', 'brikshya-portfolio'); ?></label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>"
                       name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text"
                       value="<?php echo esc_attr($title); ?>"/>
            </p>
            <?php
        }

        // font-end display of widget
        public function widget($args, $instance)
        {
            $title = apply_filters('widget_title', $instance['title']);

            echo $args['before_widget'];
            ?>
            <div id="" class="widget widget-recent-post">
                <h2 class="widget-title"><?php echo esc_html($title); ?></h2>
                <?php
                $blog_args = array(
                    'post_type' => 'blogs',
                    'posts_per_page' => '6',
                    'post_status' => 'publish',
                );
                $blog_posts = new WP_Query($blog_args);

                if ($blog_posts->have_posts()) :

                    while ($blog_posts->have_posts()) : $blog_posts->the_post(); ?>
                        <?php get_template_part('template-parts/content', 'blog-secondary');

                        ?>
                    <?php
                    endwhile;
                endif;
                wp_reset_postdata(); ?>
            </div>
            <?php

            echo $args['after_widget'];
        }

        // Updating widget replacing old instances with new
        public function update($new_instance, $old_instance)
        {
            $instance = array();
            $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
            return $instance;
        }

    }

    add_action('widgets_init', function () {
        register_widget('Brikshya_Portolio_blog_posts');
    });

}

if(!class_exists('Brikshya_Portolio_blog_category')) {
    class Brikshya_Portolio_blog_category extends WP_Widget
    {

        public function __construct()
        {
            $widget_options = array(
                'classname' => 'brikshya-portfolio-blog-category',
                'description' => 'Custom Blog Category Widget',
            );
            parent::__construct('brikshya_portfolio_blog_category', 'Brikshya Blog Category', $widget_options);
        }

        // back-end Display of widget
        public function form($instance)
        {
            if (isset($instance['title'])) {
                $title = $instance['title'];
            } else {
                $title = __('Category', 'brikshya-portfolio');
            }
// Widget admin form
            ?>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php _e('Title:', 'brikshya-portfolio'); ?></label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>"
                       name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text"
                       value="<?php echo esc_attr($title); ?>"/>
            </p>
            <?php
        }

        // font-end display of widget
        public function widget($args, $instance)
        {
            $title = apply_filters('widget_title', $instance['title']);

            echo $args['before_widget'];
            ?>
            <div id="" class="widget widget-categories">
            <?php
            $post_type = 'blogs';
            $taxonomy_category = 'blog-category';
            $catArgs = array(
                'orderby' => 'name',
                'order' => 'ASC',
                'hide_empty' => 1,
                'hierarchical' => 1,
                'taxonomy' => $taxonomy_category,
                'pad_counts' => false,
            );

            $categories = get_categories($catArgs);
            ?>
            <?php if ($categories) : ?>
            <div id="" class="widget widget-categories">
                <h2 class="widget-title"><?php echo esc_html($title); ?></h2>
                <ul>
                    <?php
                    $category = '';
                    foreach ($categories as $categorie) {
                        $category = $categorie->name;
                        $category_slug = $categorie->slug;
                        ?>

                        <li>
                            <a href="<?php echo esc_url(get_term_link($category_slug, $taxonomy_category)); ?>"><?php echo esc_html($category); ?></a>
                        </li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        <?php endif; ?>
            <?php
            echo $args['after_widget'];
        }

        // Updating widget replacing old instances with new
        public function update($new_instance, $old_instance)
        {
            $instance = array();
            $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
            return $instance;
        }

    }

    add_action('widgets_init', function () {
        register_widget('Brikshya_Portolio_blog_category');
    });
}

if(!class_exists('Brikshya_Portolio_blog_tags')) {

    class Brikshya_Portolio_blog_tags extends WP_Widget
    {

        public function __construct()
        {
            $widget_options = array(
                'classname' => 'brikshya-portfolio-blog-tags',
                'description' => 'Custom Blog Category Widget',
            );
            parent::__construct('brikshya_portfolio_blog_tags', 'Brikshya Blog Tags', $widget_options);
        }

        // back-end Display of widget
        public function form($instance)
        {
            if (isset($instance['title'])) {
                $title = $instance['title'];
            } else {
                $title = __('Tags', 'brikshya-portfolio');
            }
// Widget admin form
            ?>
            <p>
                <label for="<?php echo esc_attr($this->get_field_id('title')); ?>"><?php _e('Title:', 'brikshya-portfolio'); ?></label>
                <input class="widefat" id="<?php echo esc_attr($this->get_field_id('title')); ?>"
                       name="<?php echo esc_attr($this->get_field_name('title')); ?>" type="text"
                       value="<?php echo esc_attr($title); ?>"/>
            </p>
            <?php
        }

        // font-end display of widget
        public function widget($args, $instance)
        {
            $title = apply_filters('widget_title', $instance['title']);

            echo $args['before_widget'];
            ?>
            <div id="" class="widget widget-tag">
            <?php
            $post_type = 'blogs';
            $taxonomy_tag = 'blog-tags';
            $tagsArgs = array(
                'orderby' => 'name',
                'order' => 'ASC',
                'hide_empty' => 1,
                'hierarchical' => 1,
                'taxonomy' => $taxonomy_tag,
                'pad_counts' => false,
            );

            $tags = get_categories($tagsArgs);
            ?>
            <?php if ($tags) : ?>

            <div id="" class="widget widget-tag">
                <h2 class="widget-title"><?php echo esc_html($title); ?></h2>
                <div class="tagcloud">
                    <?php
                    $tag = '';
                    foreach ($tags as $tag) {
                        $tags = $tag->name;
                        $tag_slug = $tag->slug;
                        ?>
                        <a href="<?php echo esc_url(get_term_link($tag_slug, $taxonomy_tag)); ?>"><?php echo esc_html($tags); ?></a>
                        <?php
                    }
                    ?>
                </div>
            </div>
        <?php endif; ?>
            <?php
            echo $args['after_widget'];
        }

        // Updating widget replacing old instances with new
        public function update($new_instance, $old_instance)
        {
            $instance = array();
            $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
            return $instance;
        }

    }

    add_action('widgets_init', function () {
        register_widget('Brikshya_Portolio_blog_tags');
    });
}