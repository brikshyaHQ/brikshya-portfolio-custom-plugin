=== Brikshya Portfolio CPT ===
Contributors: brikshyatechnology
Donate link: brikshya.com
Tags: Custom Post Type and Taxonomy
Requires at least: 3.0.1
Tested up to: 3.4
Stable tag: 4.3
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This plugin is exclusively for brikshya portfolio theme.
This plugin generates require custom post types and their relative taxonomy.

== Description ==

Brikshya Portfolio CPT plugin generates custom post type Blogs with relative taxonomy, Experiences, Services,
and portfolio with relative taxonomy.

== Installation ==

1. Upload `brikshya-portfolio-cpt` folder to the `/wp-content/plugins/` directory
1. Activate the plugin through the 'Plugins' menu in WordPress

== Frequently Asked Questions ==

== Screenshots ==

== Changelog ==

== Upgrade Notice ==
