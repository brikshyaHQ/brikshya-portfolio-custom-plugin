<?php

/**
 * Fired during plugin deactivation
 *
 * @link       brikshya.com
 * @since      1.0.0
 *
 * @package    Brikshya_Portfolio_Cpt
 * @subpackage Brikshya_Portfolio_Cpt/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Brikshya_Portfolio_Cpt
 * @subpackage Brikshya_Portfolio_Cpt/includes
 * @author     Brikshya <mail@brikshya.com>
 */
class Brikshya_Portfolio_Cpt_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
