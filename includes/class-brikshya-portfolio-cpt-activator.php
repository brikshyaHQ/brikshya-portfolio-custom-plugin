<?php

/**
 * Fired during plugin activation
 *
 * @link       brikshya.com
 * @since      1.0.0
 *
 * @package    Brikshya_Portfolio_Cpt
 * @subpackage Brikshya_Portfolio_Cpt/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Brikshya_Portfolio_Cpt
 * @subpackage Brikshya_Portfolio_Cpt/includes
 * @author     Brikshya <mail@brikshya.com>
 */
class Brikshya_Portfolio_Cpt_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

    }

}
