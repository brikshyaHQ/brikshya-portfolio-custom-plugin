<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       brikshya.com
 * @since      1.0.0
 *
 * @package    Brikshya_Portfolio_Cpt
 * @subpackage Brikshya_Portfolio_Cpt/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Brikshya_Portfolio_Cpt
 * @subpackage Brikshya_Portfolio_Cpt/includes
 * @author     Brikshya <mail@brikshya.com>
 */
class Brikshya_Portfolio_Cpt_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {

		load_plugin_textdomain(
			'brikshya-portfolio-cpt',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);

	}



}
